import { style, composeStyles } from '@vanilla-extract/css';
import { atoms } from '../../src/styles/atoms.css';

export const card = composeStyles(
  atoms({
    background: 'green-50',
    // borderRadius: {
    //   mobile: '4x',
    //   desktop: '5x',
    // },
    padding: {
      mobile: '7x',
      tablet: '6x',
      desktop: '8x',
    },
  }),
  style({
    // transition: 'transform 4s ease-in-out',
    // ':hover': {
    //   cursor: 'default',
    //   transform: 'scale(2) rotate(720deg)',
    // },
  })
);
