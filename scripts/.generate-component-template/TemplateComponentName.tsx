import * as styles from './TemplateComponentName.css';
import { FC } from 'react';

export const TemplateComponentName: FC = ({ children }) => (
  <div className={styles.card}>
    {'<TemplateComponentName>'}
    {children}
    {'</TemplateComponentName>'}
  </div>
);
