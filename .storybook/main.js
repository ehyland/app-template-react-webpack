const { VanillaExtractPlugin } = require('@vanilla-extract/webpack-plugin');
const { src } = require('../config/paths');

module.exports = {
  stories: ['../src/**/*.stories.mdx', '../src/**/*.stories.@(js|jsx|ts|tsx)'],
  addons: ['@storybook/addon-links', '@storybook/addon-essentials'],
  core: {
    builder: 'webpack5',
  },
  webpackFinal: async (config, { configType }) => {
    config.plugins.push(new VanillaExtractPlugin());
    config.resolve.modules = ['node_modules', src];
    config.resolve.extensions = ['.ts', '.tsx', '.js', '.jsx', '.json'];
    return config;
  },
};
