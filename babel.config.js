/**
 * @type {import('@babel/core').ConfigFunction}
 */
module.exports = (api) => {
  return {
    plugins: ['@vanilla-extract/babel-plugin'],
    presets: [
      [
        '@babel/preset-env',
        {
          useBuiltIns: 'entry',
          corejs: 3,
          exclude: ['transform-typeof-symbol'],
        },
      ],
      [
        '@babel/preset-react',
        {
          runtime: 'automatic',
          development: api.env('development'),
        },
      ],
      ['@babel/preset-typescript'],
    ],
  };
};
