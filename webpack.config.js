const { compact } = require('lodash');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { VanillaExtractPlugin } = require('@vanilla-extract/webpack-plugin');
const { HotModuleReplacementPlugin } = require('webpack');
const path = require('path');

const paths = {
  src: path.resolve('src'),
  dist: path.resolve('dist'),
  entryRelative: './src/index.tsx',
  entry: path.resolve('./src/index.tsx'),
};

/**
 * @returns {import('webpack').Configuration}
 */
module.exports = ({ WEBPACK_SERVE = false }) => {
  const IS_DEV = process.env.NODE_ENV === 'development';
  const IS_HOT = WEBPACK_SERVE;
  return {
    stats: 'summary',
    mode: IS_DEV ? 'development' : 'production',
    devtool: IS_DEV ? 'eval-source-map' : 'source-map',
    entry: paths.entryRelative,
    output: {
      path: paths.dist,
    },
    devServer: {
      open: false,
      contentBase: paths.dist,
      host: 'localhost',
      hot: IS_HOT,
      injectClient: IS_HOT,
    },
    plugins: compact([
      IS_HOT && new HotModuleReplacementPlugin(),
      new VanillaExtractPlugin(),
      new MiniCssExtractPlugin(),
      new HtmlWebpackPlugin({ template: 'src/index.html' }),
    ]),
    resolve: {
      modules: ['node_modules', paths.src],
      extensions: ['.ts', '.tsx', '.js', '.jsx', '.json'],
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx|ts|tsx)$/,
          loader: 'babel-loader',
        },
        {
          test: /\.css$/i,
          use: [MiniCssExtractPlugin.loader, 'css-loader'],
        },
        {
          test: /\.(eot|svg|ttf|woff|woff2|png|jpg|gif)$/,
          type: 'asset',
        },
      ],
    },
  };
};
