import { createGlobalTheme } from '@vanilla-extract/css';
import * as colors from './colors';

const grid = 4;
const px = (value: string | number) => `${value}px`;

export const breakpoints = {
  mobile: 0,
  tablet: 768,
  desktop: 1200,
};

const spacing = {
  xsmall: px(1 * grid),
  small: px(2 * grid),
  medium: px(3 * grid),
  large: px(5 * grid),
  xlarge: px(8 * grid),
  xxlarge: px(12 * grid),
  xxxlarge: px(24 * grid),
};

const negativeSpacing = {
  ['-xsmall']: `-${spacing.xsmall}`,
  ['-small']: `-${spacing.small}`,
  ['-medium']: `-${spacing.medium}`,
  ['-large']: `-${spacing.large}`,
  ['-xlarge']: `-${spacing.xlarge}`,
  ['-xxlarge']: `-${spacing.xxlarge}`,
  ['-xxxlarge']: `-${spacing.xxxlarge}`,
};

export const vars = createGlobalTheme(':root', {
  fonts: {
    body: '-apple-system, BlinkMacSystemFont, Roboto, "Helvetica Neue", HelveticaNeue, Helvetica, Arial, sans-serif',
  },
  grid: px(grid),
  space: {
    none: '0',
    ...spacing,
    ...negativeSpacing,
  },
  size: {
    xsmall: px(480),
    small: px(600),
    standard: px(740),
    large: px(1350),
    screenWidth: '100vw',
    screenHeight: '100vw',
  },
  heading: {
    h1: {
      mobile: {
        fontSize: px(36),
      },
      tablet: {
        fontSize: px(52),
      },
      desktop: {
        fontSize: px(52),
      },
    },
    h2: {
      mobile: {
        fontSize: px(28),
      },
      tablet: {
        fontSize: px(38),
      },
      desktop: {
        fontSize: px(38),
      },
    },
    h3: {
      mobile: {
        fontSize: px(24),
      },
      tablet: {
        fontSize: px(30),
      },
      desktop: {
        fontSize: px(30),
      },
    },
    h4: {
      mobile: {
        fontSize: px(22),
      },
      tablet: {
        fontSize: px(22),
      },
      desktop: {
        fontSize: px(22),
      },
    },
  },
  text: {
    standard: {
      mobile: {
        fontSize: px(18),
      },
      tablet: {
        fontSize: px(20),
      },
      desktop: {
        fontSize: px(20),
      },
    },

    code: {
      mobile: {
        fontSize: px(13),
      },
      tablet: {
        fontSize: px(15),
      },
      desktop: {
        fontSize: px(15),
      },
    },

    small: {
      mobile: {
        fontSize: px(16),
      },
      tablet: {
        fontSize: px(16),
      },
      desktop: {
        fontSize: px(16),
      },
    },

    xsmall: {
      mobile: {
        fontSize: px(15),
      },
      tablet: {
        fontSize: px(15),
      },
      desktop: {
        fontSize: px(15),
      },
    },
  },
  weight: {
    regular: '400',
    strong: '700',
  },
  color: colors,
  border: {
    width: {
      standard: px(1 * grid),
      large: px(2 * grid),
    },
    radius: {
      small: px(2 * grid),
      medium: px(4 * grid),
      large: px(7 * grid),
      full: '9999px',
    },
  },
  shadows: {
    default: '0 5px 10px 0 rgba(0, 0, 0, 0.1)',
  },
});
