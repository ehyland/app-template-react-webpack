import { createAtomicStyles, createAtomsFn } from '@vanilla-extract/sprinkles';
import { breakpoints, vars } from 'styles/vars.css';

const responsiveStyles = createAtomicStyles({
  defaultCondition: 'mobile',
  conditions: {
    mobile: {},
    tablet: { '@media': `screen and (min-width: ${breakpoints.tablet})` },
    desktop: { '@media': `screen and (min-width: ${breakpoints.desktop})` },
  },
  properties: {
    color: vars.color,
    background: vars.color,
    display: ['none', 'flex', 'block', 'inline', 'run-in'],
    flexDirection: ['row', 'column'],
    flexWrap: ['wrap', 'nowrap'],
    alignItems: ['stretch', 'flex-start', 'center', 'flex-end'],
    justifyContent: ['stretch', 'flex-start', 'center', 'flex-end', 'space-between'],
    gap: vars.space,
    paddingTop: vars.space,
    paddingBottom: vars.space,
    paddingLeft: vars.space,
    paddingRight: vars.space,
    marginTop: vars.space,
    marginBottom: vars.space,
    marginLeft: vars.space,
    marginRight: vars.space,
    width: vars.size,
    height: vars.size,
    borderRadius: vars.border.radius,
    fontFamily: vars.fonts,
    textAlign: ['center'],
    boxShadow: vars.shadows,
  },
  shorthands: {
    padding: ['paddingTop', 'paddingBottom', 'paddingLeft', 'paddingRight'],
    paddingX: ['paddingLeft', 'paddingRight'],
    paddingY: ['paddingTop', 'paddingBottom'],
    margin: ['marginTop', 'marginBottom', 'marginLeft', 'marginRight'],
    marginX: ['marginLeft', 'marginRight'],
    marginY: ['marginTop', 'marginBottom'],
  },
});

export const atoms = createAtomsFn(responsiveStyles);
