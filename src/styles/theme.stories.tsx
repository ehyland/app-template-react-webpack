import { Meta } from '@storybook/react';
import { Box } from 'components/Box';
import { Fragment } from 'react';
import { vars } from 'styles/vars.css';
import { groupBy, startCase } from 'lodash';

export default {
  title: 'Example/Theme',
} as Meta;

const grouped = groupBy(Object.keys(vars.color), (key) => {
  const [, group] = key.match(/^([a-z]+)\d+$/i) ?? [];

  if (group) {
    return group;
  } else {
    return '';
  }
});

const Template = () => (
  <Box>
    {Object.entries(grouped).map(([group, colors]) => (
      <Fragment key={group}>
        <Box>{startCase(group)}</Box>
        <Box display="flex" flexWrap="wrap" gap="medium" marginBottom="medium">
          {colors.map((color) => (
            <Box
              key={color}
              boxShadow="default"
              style={{
                width: '100px',
                height: '100px',
              }}
              background={color as any}
            />
          ))}
        </Box>
      </Fragment>
    ))}
  </Box>
);

export const Primary = Template.bind({});
