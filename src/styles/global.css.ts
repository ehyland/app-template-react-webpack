import { globalStyle } from '@vanilla-extract/css';
import { normalize } from 'polished';
import { vars } from './vars.css';

normalize().forEach((rules) => {
  Object.entries(rules).forEach(([selector, styles]) => {
    globalStyle(selector, styles as any);
  });
});

globalStyle('body', {
  fontFamily: vars.fonts.body,
});
