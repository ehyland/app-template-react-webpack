import { style, composeStyles, styleVariants } from '@vanilla-extract/css';
import { atoms } from 'styles/atoms.css';
import { vars } from 'styles/vars.css';

export const buttonVarient = styleVariants({
  primary: {},
  secondary: {},
});

export const button = style({
  appearance: 'none',
  margin: 0,
  display: 'inline-block',
  border: vars.border.width.standard,
  borderColor: 'transparent',
  borderRadius: vars.border.radius.medium,
  textTransform: 'uppercase',
  padding: `${vars.space.medium} ${vars.space.medium}`,
  background: vars.color.amber200,
  color: vars.color.white,
});
