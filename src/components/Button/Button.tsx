import * as styles from './Button.css';
import { PropsWithChildren } from 'react';

export type ButtonProps = PropsWithChildren<{}>;

export const Button = ({ children }: ButtonProps) => (
  <button className={styles.button}>{children}</button>
);
