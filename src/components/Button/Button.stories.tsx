import { Story, Meta } from '@storybook/react';
import { ComponentProps } from 'react';
import { vars } from 'styles/vars.css';
import { Button, ButtonProps } from './Button';

export default {
  title: 'Example/Button',
  component: Button,
  argTypes: {
    // background: { control: 'select', options: Object.keys(vars.color) },
    // width: { control: 'select', options: Object.keys(vars.size) },
    // height: { control: 'select', options: Object.keys(vars.size) },
  },
} as Meta;

const Template: Story<ButtonProps> = (args) => <Button {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  children: 'Hello world',
};
