import { Story, Meta } from '@storybook/react';
import { ComponentProps } from 'react';
import { vars } from 'styles/vars.css';
import { Box } from './Box';

export default {
  title: 'Example/Box',
  component: Box,
  argTypes: {
    background: { control: 'select', options: Object.keys(vars.color) },
    width: { control: 'select', options: Object.keys(vars.size) },
    height: { control: 'select', options: Object.keys(vars.size) },
  },
} as Meta;

const Template: Story<ComponentProps<typeof Box>> = (args) => <Box {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  background: 'blue400',
  width: 'standard',
  height: 'standard',
};
