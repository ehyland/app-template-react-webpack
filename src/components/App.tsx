import { Box } from './Box';

export const App = () => (
  <Box background="blue400">
    <Box as="h1" paddingX="small">
      Hello world
    </Box>
  </Box>
);
